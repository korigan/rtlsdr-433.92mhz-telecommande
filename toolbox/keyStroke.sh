#!/bin/bash

pipe=/tmp/ororo

trap "rm -f $pipe" EXIT

if [[ ! -p $pipe ]]; then
    mkfifo $pipe
    chmod ugo+rwx /tmp/ororo
fi

#Pour connaitre le code dd'une touche, utiliser: xev


while true
do
    if read line <$pipe; then
        if [[ "$line" == 'quit' ]]; then
            break
        fi
        if [[ "$line" == 'subtitle' ]]; then
            xdotool key s
        fi
        if [[ "$line" == 'left' ]]; then
            xdotool key Left
        fi
        if [[ "$line" == 'traduction' ]]; then
            xdotool key t
        fi
        echo $line
    fi
done

echo "Reader exiting"
