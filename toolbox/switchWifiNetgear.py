#!/usr/bin/python
import requests, sys
from requests.auth import HTTPBasicAuth

if(not len(sys.argv)>1):
    print("Usage: %s up|down" % sys.argv[0])
    sys.exit(2)

wifi={}

wifiDown={'ServiceSetIdentifier':'<obfuscatedESSID>',
'ChannelNumber':11,
'NetgearClosedNetwork':'0x01',
'NetgearSecOptionIndex':1,
'NetgearSecOption1':1,
'SharedKeyAuthentication':0,
'w802_1xAuthentication':0,
'WEP64_WepPassPhrase':'<obfuscatedWPA_keys>',
'WEP64_GenerateWepKeys':'',
'WEP64_WepKeysGenerated':'',
'WEP64_AdvWirelessSetupKeyIndex':'',
'WEP64_AdvWirelessSetupKey1':1,
'&WEP64_AdvWirelessSetupWepKey1':'0000000000',
'&WEP64_AdvWirelessSetupWepKey2':'0000000000',
'&WEP64_AdvWirelessSetupWepKey3':'0000000000',
'WEP64_AdvWirelessSetupWepKey4':'0000000000',
'WepPassPhrase':'<obfuscatedWPA_keys>',
'DefaultSecretKey':1,
'WepKeysGenerated':63,
'WEP128_WepPassPhrase':'<obfuscatedWPA_keys>',
'WEP128_GenerateWepKeys':'',
'WEP128_WepKeysGenerated':'',
'WEP128_AdvWirelessSetupKeyIndex':'',
'WEP128_AdvWirelessSetupKey1':1,
'WEP128_AdvWirelessSetupWepKey1':'',
'WEP128_AdvWirelessSetupWepKey2':'',
'WEP128_AdvWirelessSetupWepKey3':'',
'WEP128_AdvWirelessSetupWepKey4':'',
'RadiusServer1':'0.0.0.0',
'RadiusPort1':1812,
'RadiusKey1':'',
'WpaPskEncryption':1,
'WpaPreSharedKey':'<obfuscatedWPA_keys>',
'WpaEncryption':1,
'RadiusServer':'0.0.0.0',
'RadiusPort':1812,
'RadiusKey':'',
'GenerateWepKeys':0,
'ChangeSecOption':'',
'commitwlanSecurity':1}



wifiUp={'NetgearEnableWireless':'0x01',
'NetgearSecOptionIndex':4,
'NetgearSecOption4':4,
'WEP64_WepPassPhrase':'',
'WEP64_GenerateWepKeys':'',
'WEP64_WepKeysGenerated':'',
'WEP64_AdvWirelessSetupKeyIndex':'',
'WEP64_AdvWirelessSetupKey1':1,
'WEP64_AdvWirelessSetupWepKey1':'0000000000',
'WEP64_AdvWirelessSetupWepKey2':'0000000000',
'WEP64_AdvWirelessSetupWepKey3':'0000000000',
'WEP64_AdvWirelessSetupWepKey4':'0000000000',
'WepPassPhrase':'<obfuscatedWPA_keys>',
'DefaultSecretKey':1,
'WepKeysGenerated':63,
'WEP128_WepPassPhrase':'',
'WEP128_GenerateWepKeys':'',
'WEP128_WepKeysGenerated':'',
'WEP128_AdvWirelessSetupKeyIndex':'',
'WEP128_AdvWirelessSetupKey1':1,
'WEP128_AdvWirelessSetupWepKey1':'',
'WEP128_AdvWirelessSetupWepKey2':'',
'WEP128_AdvWirelessSetupWepKey3':'',
'WEP128_AdvWirelessSetupWepKey4':'',
'RadiusServer1':'0.0.0.0',
'RadiusPort1':1812,
'RadiusKey1':'',
'WpaPskEncryption':1,
'WpaPreSharedKey':'<obfuscatedWPA_keys>',
'WpaEncryption':1,
'RadiusServer':'0.0.0.0',
'RadiusPort':1812,
'RadiusKey':'',
'GenerateWepKeys':0,
'ChangeSecOption':'',
'commitwlanSecurity':1}


if("up" in sys.argv[1]):
	wifi=wifiUp
else:
	wifi=wifiDown

AUTHED = HTTPBasicAuth('admin', '<obfuscatedWPA_keys>')
r = requests.post("http://192.168.0.1/goform/NetGearRg802dot11BasicCfg", data=wifi, auth=AUTHED)
print(r.status_code, r.reason)
